package noxqs.hzzo;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by Noxqs on 13/12/2014.
 */
public class FuncFragment extends Fragment {



    public static final String FUNC_STRING = "functionalities_string";

    public String getFUNC_STRING() {
        return FUNC_STRING;
    }


    @Override
    public View onCreateView(LayoutInflater inflater,ViewGroup container, Bundle savedInstanceState) {
        int i = getArguments().getInt(FUNC_STRING);
        if(i==0){
            View rootView = inflater.inflate(R.layout.activity_success_rate, container, false);
            String functions = getResources().getStringArray(R.array.functionalities_drawer)[i];
            getActivity().setTitle(functions);
            return rootView;
        }
        else if(i==1){
            View rootView = inflater.inflate(R.layout.activity_locations_hzzo, container, false);
            String functions = getResources().getStringArray(R.array.functionalities_drawer)[i];
            getActivity().setTitle(functions);
            return rootView;
        }

        return null;
    }
}

