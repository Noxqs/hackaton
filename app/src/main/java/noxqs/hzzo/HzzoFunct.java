package noxqs.hzzo;


import android.annotation.TargetApi;
import android.app.Dialog;
import android.app.Fragment;
import android.app.FragmentManager;

import android.app.ProgressDialog;
import android.content.ClipData;
import android.content.Context;
import android.content.res.Configuration;

import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.os.StrictMode;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.io.input.BOMInputStream;
import org.apache.http.util.ByteArrayBuffer;
import org.w3c.dom.Text;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.StandardCharsets;
import java.util.EmptyStackException;
import java.util.concurrent.ExecutionException;

import hr.foi.mtlab.libs.bnetwork.MTDataFetcher;


public class HzzoFunct extends ActionBarActivity implements AdapterView.OnItemClickListener{

    private ListView listView;
    private DrawerLayout drawerLayout;
    private MyAdapter myAdapter;
    private ActionBarDrawerToggle drawerListener;
    private String[] functions;


    public static final String file_url = "http://www.hzzo-net.hr/hackathon/OM_da_ne30112014_1.csv";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hzzo_funct);

        listView = (ListView) findViewById(R.id.drawerlist);
        myAdapter = new MyAdapter(this);
        listView.setAdapter(myAdapter);
        listView.setOnItemClickListener(this);
        drawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout);
        drawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);
        drawerListener = new ActionBarDrawerToggle(this, drawerLayout, R.drawable.ic_drawer, R.string.open_nav, R.string.close_nav){
            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
            }
        };


        drawerLayout.setDrawerListener(drawerListener);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
    }

    public void setTitle(String title){
        getSupportActionBar().setTitle(title);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_hzzo_funct, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.


        //noinspection SimplifiableIfStatement
        if (drawerListener.onOptionsItemSelected(item)) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState){
        super.onPostCreate(savedInstanceState);
        drawerListener.syncState();
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        selectItem(position);
    }



    private void selectItem(int position) {

        FuncFragment fragment = new FuncFragment();
        Bundle args = new Bundle();
        args.putInt(FuncFragment.FUNC_STRING, position);
        fragment.setArguments(args);

        FragmentManager fragmentManager = getFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.mainContent, fragment).commit();

        listView.setItemChecked(position,true);
        setTitle(functions[position]);
        drawerLayout.closeDrawer(listView);

    }


    @Override
    public void onConfigurationChanged(Configuration newConfig){
        super.onConfigurationChanged(newConfig);
        drawerListener.onConfigurationChanged(newConfig);
    }



    public class FuncFragment extends Fragment {
        public static final String FUNC_STRING = "functionalities";

        public FuncFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater,ViewGroup container, Bundle savedInstanceState) {
            int i = getArguments().getInt(FUNC_STRING);
            if(i==0){
                View rootView = inflater.inflate(R.layout.activity_success_rate, container, false);
                String functions = getResources().getStringArray(R.array.functionalities_drawer)[i];
                getActivity().setTitle(functions);
                new CSVSomething().execute(file_url);
                return rootView;
            }
            else if(i==1){
                View rootView = inflater.inflate(R.layout.activity_locations_hzzo, container, false);
                String functions = getResources().getStringArray(R.array.functionalities_drawer)[i];
                getActivity().setTitle(functions);
                return rootView;
            }
            else if(i==2){
                View rootView = inflater.inflate(R.layout.patient_number, container, false);

                String functions = getResources().getStringArray(R.array.functionalities_drawer)[i];
                getActivity().setTitle(functions);
                return rootView;
            }
            else if(i==3){
                View rootView = inflater.inflate(R.layout.ordinationlimits, container, false);
                String functions = getResources().getStringArray(R.array.functionalities_drawer)[i];
                getActivity().setTitle(functions);
                return rootView;
            }
            return null;
        }
    }

    public class MyAdapter extends BaseAdapter{

        private Context context;
        int[] images = {R.drawable.logo, R.drawable.logo, R.drawable.logo, R.drawable.logo};

        public MyAdapter(Context context){
            this.context = context;
            functions = context.getResources().getStringArray(R.array.functionalities_drawer);
        }


        @Override
        public int getCount() {
            return functions.length;
        }

        @Override
        public Object getItem(int position) {
            return functions[position];
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View row = null;
            if(convertView == null){

                LayoutInflater inflater= (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                row = inflater.inflate(R.layout.custom_row,parent,false);

            }else
            {
                row = convertView;
            }
            TextView titleTextView = (TextView) row.findViewById(R.id.row_textview);
            ImageView titleImageView = (ImageView) row.findViewById(R.id.nav_drawer_icon1);

            titleTextView.setText(functions[position]);
            titleImageView.setImageResource(images[position]);

            return row;
        }
    }

    public class CSVSomething extends AsyncTask<String, String, String>{
        private ItemAdapter itemArrayAdapter;
        @Override
        protected String doInBackground(String... f_url) {
            int count;

            try {
                URL url = new URL(f_url[0]);
                URLConnection connection = url.openConnection();
                connection.connect();


                InputStream inputStream = connection.getInputStream();
                int lengthOfFile = inputStream.available();

                File storageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
                if (!storageDir.exists()) {
                    storageDir.mkdirs();
                }
                File file = new File(storageDir.getAbsolutePath() + File.separator + "test.csv");
                byte data[] = new byte[1024];
                long total = 0;
                FileOutputStream fos = new FileOutputStream(file);
                while ((count = inputStream.read(data)) != -1) {
                    total += count;
                    publishProgress("" + (int) ((total*100) / lengthOfFile));
                    fos.write(data, 0, count);
                }

                fos.flush();
                CSVReader objekt = new CSVReader(inputStream);
                objekt.read();
                itemArrayAdapter = new ItemAdapter(getBaseContext(), R.layout.activity_locations_hzzo);

                fos.close();
                inputStream.close();






            } catch (Exception e) {
                Log.e("Error", e.getMessage());
            }


            return null;
        }
     }



}


