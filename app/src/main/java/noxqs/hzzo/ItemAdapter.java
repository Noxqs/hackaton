package noxqs.hzzo;

import android.content.Context;
import android.support.v7.internal.view.menu.MenuView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Noxqs on 14/12/2014.
 */
public class ItemAdapter extends ArrayAdapter {
    private List<String[]> list = new ArrayList<String[]>();

    static class ViewHolder{
        TextView name;
    }

    public ItemAdapter(Context context, int resource) {
        super(context, resource);

        Log.d("smece", "uso sam u itemadapter konstruktor");
    }

    public void add(String[] object) {
        super.add(object);
        list.add(object);
        Log.d("smece", "u addu sam");
    }

    @Override
    public int getCount() {
        return this.list.size();
    }

    @Override
    public Object getItem(int position) {
        add(this.list.get(position));
        return this.list.get(position);

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View row = convertView;
        ItemViewHolder viewHolder;
        if(row == null){
            LayoutInflater inflater = (LayoutInflater) this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = inflater.inflate(R.layout.activity_locations_hzzo, parent, false);
            viewHolder = new ItemViewHolder();
            viewHolder.name = (TextView) row.findViewById(R.id.text_populate);
            row.setTag(viewHolder);
        } else {
            viewHolder = (ItemViewHolder) row.getTag();
        }

        String[] stat = (String[]) getItem(position);
        viewHolder.name.setText(stat[0]);
        return row;
    }
}
