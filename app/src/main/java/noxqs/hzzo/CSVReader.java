package noxqs.hzzo;

import android.content.ClipData;
import android.content.Context;
import android.os.Environment;
import android.util.Log;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Noxqs on 14/12/2014.
 */


public class CSVReader{

    InputStream inputStream;

    public CSVReader(InputStream is) {
        this.inputStream = is;
        Log.d("smece", "uso u Csv reader");

    }

    public List<String[]> read(){

        List<String[]> resultList = new ArrayList<String[]>();
        Log.d("smece", "uso u List<String>");

        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
        try{
            Log.d("smece", "uso u try");
            String csvline;
            while((csvline = reader.readLine()) != null ){
                String[] row = csvline.split(";");
                resultList.add(row);
                Log.d("smece","u whileu sam");

            }
        } catch (IOException e) {
            throw new RuntimeException("Error reading CSV file" + e);
        } finally
        {
            try
            {
                Log.d("smece", "ispred input streamclose");
                inputStream.close();
                Log.d("smece", "iza input streamclose");

            }
            catch (IOException e)
            {
                 throw new RuntimeException("Error while closing input stream" + e);
            }
        }
        return resultList;

    }



}
